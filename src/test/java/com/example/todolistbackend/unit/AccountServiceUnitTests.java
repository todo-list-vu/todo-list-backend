package com.example.todolistbackend.unit;

import com.example.todolistbackend.account.AccountRepository;
import com.example.todolistbackend.account.AccountService;
import com.example.todolistbackend.account.util.AccountValidator;
import com.example.todolistbackend.account.entity.AccountEntity;
import com.example.todolistbackend.util.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountServiceUnitTests {
  @Mock AccountRepository repository;
  @Mock AccountValidator validator;
  @InjectMocks AccountService service;

  @Test
  void givenId_whenFindById_thenReturnEntity() {
    AccountEntity expected = TestDataFactory.getAccount();

    when(repository.findById(expected.getId()))
        .thenReturn(Optional.of(TestDataFactory.getAccount()));

    AccountEntity actual = service.findById(expected.getId());

    Assertions.assertEquals(expected, actual);
    verify(validator, times(1)).existsBy(expected.getId(), "err.account.find.notFound");
    verify(repository, times(1)).findById(expected.getId());
  }

  @Test
  void givenValidEntity_whenCreate_thenGetEntityWithId() {
    AccountEntity expected = TestDataFactory.getAccount().setId(null);

    when(repository.save(expected)).thenReturn(expected.setId(1L));

    AccountEntity actual = service.create(expected);

    Assertions.assertEquals(expected, actual);
    verify(validator, times(1)).notExistsBy(expected.getName(), "err.account.create.duplicate");
    verify(repository, times(1)).save(expected);
  }

  @Test
  void givenValidEntity_whenUpdate_thenGetEntityWithId() {
    AccountEntity expected = TestDataFactory.getAccount();

    when(repository.save(expected)).thenReturn(expected.setId(1L));

    AccountEntity actual = service.update(expected);

    Assertions.assertEquals(expected, actual);
    verify(validator, times(1)).existsBy(expected.getId(), "err.account.find.notFound");
    verify(repository, times(1)).save(expected);
  }

  @Test
  void givenId_whenDelete_thenReturnEntity() {
    AccountEntity expected = TestDataFactory.getAccount();

    service.delete(expected.getId());

    verify(validator, times(1)).existsBy(expected.getId(), "err.account.find.notFound");
    verify(repository, times(1)).deleteById(expected.getId());
  }
}
