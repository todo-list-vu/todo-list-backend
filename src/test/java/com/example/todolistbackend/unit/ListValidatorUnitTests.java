package com.example.todolistbackend.unit;

import com.example.todolistbackend.exception.ApiException;
import com.example.todolistbackend.list.TodoListRepository;
import com.example.todolistbackend.list.entity.TodoListEntity;
import com.example.todolistbackend.list.util.TodoListValidator;
import com.example.todolistbackend.util.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ListValidatorUnitTests {
  @Mock TodoListRepository repository;
  @InjectMocks TodoListValidator validator;

  @Test
  void givenExistingId_whenExistBy_thenDoNotThrow() {
    TodoListEntity entity = TestDataFactory.getTodoList();
    String message = "err.todolist.find.notFound";

    Mockito.when(repository.existsById(entity.getId())).thenReturn(true);
    Assertions.assertDoesNotThrow(() -> validator.existsBy(entity.getId(), message));
  }

  @Test
  void givenNotExistingName_whenCallDoesNotExistBy_whenDoNotThrow() {
    TodoListEntity entity = TestDataFactory.getTodoList();
    String message = "err.todolist.find.notFound";

    Mockito.when(repository.existsByName(entity.getName())).thenReturn(false);
    Assertions.assertDoesNotThrow(() -> validator.notExistsBy(entity.getName(), message));
  }

  @Test
  void givenNotExistingId_whenExistsBy_thenThrowException() {
    TodoListEntity entity = TestDataFactory.getTodoList();
    String message = "err.todolist.find.notFound";
    String expectedErrorMessage = String.format("%s id=%d", message, entity.getId());

    Mockito.when(repository.existsById(entity.getId())).thenReturn(false);
    Throwable exception =
        Assertions.assertThrows(
            ApiException.class, () -> validator.existsBy(entity.getId(), message));
    Assertions.assertEquals(expectedErrorMessage, exception.getMessage());
  }

  @Test
  void givenExistingName_whenNotExistBy_thenThrowException() {
    TodoListEntity entity = TestDataFactory.getTodoList();
    String message = "err.todolist.find.notFound";
    String expectedErrorMessage = String.format("%s name=%s", message, entity.getName());

    Mockito.when(repository.existsByName(entity.getName())).thenReturn(true);
    Throwable exception =
        Assertions.assertThrows(
            ApiException.class, () -> validator.notExistsBy(entity.getName(), message));
    Assertions.assertEquals(expectedErrorMessage, exception.getMessage());
  }
}
