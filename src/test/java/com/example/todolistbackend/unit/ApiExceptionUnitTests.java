package com.example.todolistbackend.unit;

import com.example.todolistbackend.exception.ApiException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ApiExceptionUnitTests {
  @Test
  public void givenValidException_whenGetMessage_thenCorrectMessage() {
    ApiException exception =
        new ApiException("err.feature.creation", HttpStatus.INTERNAL_SERVER_ERROR)
            .addLabel("label", 123)
            .addLabel("label2", "hello")
            .addLabel("label3", Arrays.asList("1", 2, 3));

    String expected = "err.feature.creation label=123, label2=hello, label3=[1, 2, 3]";
    String actual = exception.getMessage();

    assertEquals(expected, actual);
  }

  @Test
  void givenValidNotFoundException_whenCallGetStatus_thenStatusIsCorrect() {
    ApiException exception = ApiException.notFound("message");

    HttpStatus expected = HttpStatus.NOT_FOUND;
    HttpStatus actual = exception.getStatus();

    assertEquals(expected, actual);
  }

  @Test
  void givenValidBadException_whenCallGetStatus_thenStatusIsCorrect() {
    ApiException exception = ApiException.bad("message");

    HttpStatus expected = HttpStatus.BAD_REQUEST;
    HttpStatus actual = exception.getStatus();

    assertEquals(expected, actual);
  }

  @Test
  void givenValidInternalErrorException_whenCallGetStatus_thenStatusIsCorrect() {
    ApiException exception = ApiException.internalError("message");

    HttpStatus expected = HttpStatus.INTERNAL_SERVER_ERROR;
    HttpStatus actual = exception.getStatus();

    assertEquals(expected, actual);
  }

  @Test
  void givenValidConflictException_whenCallGetStatus_thenStatusIsCorrect() {
    ApiException exception = ApiException.conflict("message");

    HttpStatus expected = HttpStatus.CONFLICT;
    HttpStatus actual = exception.getStatus();

    assertEquals(expected, actual);
  }
}
