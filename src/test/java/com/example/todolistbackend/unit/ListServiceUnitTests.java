package com.example.todolistbackend.unit;

import com.example.todolistbackend.list.TodoListRepository;
import com.example.todolistbackend.list.TodoListService;
import com.example.todolistbackend.list.entity.TodoListEntity;
import com.example.todolistbackend.list.util.TodoListValidator;
import com.example.todolistbackend.util.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ListServiceUnitTests {
  @Mock TodoListRepository repository;
  @Mock TodoListValidator validator;
  @InjectMocks TodoListService service;

  @Test
  void givenId_whenFindById_thenReturnEntity() {
    TodoListEntity expected = TestDataFactory.getTodoList();

    when(repository.findById(expected.getId()))
        .thenReturn(Optional.of(TestDataFactory.getTodoList()));

    TodoListEntity actual = service.findById(expected.getId());

    Assertions.assertEquals(expected, actual);
    verify(validator, times(1)).existsBy(expected.getId(), "err.account.find.notFound");
    verify(repository, times(1)).findById(expected.getId());
  }

  @Test
  void givenValidEntity_whenCreate_thenGetEntityWithId() {
    TodoListEntity expected = TestDataFactory.getTodoList().setId(null);

    when(repository.save(expected)).thenReturn(expected.setId(1L));

    TodoListEntity actual = service.create(expected);

    Assertions.assertEquals(expected, actual);
    verify(validator, times(1)).notExistsBy(expected.getName(), "err.account.create.duplicate");
    verify(repository, times(1)).save(expected);
  }

  @Test
  void givenValidEntity_whenUpdate_thenGetEntityWithId() {
    TodoListEntity expected = TestDataFactory.getTodoList();

    when(repository.findById(expected.getId()))
        .thenReturn(Optional.of(TestDataFactory.getTodoList()));
    when(repository.save(expected)).thenReturn(expected.setId(1L));

    TodoListEntity actual = service.update(expected);

    Assertions.assertEquals(expected, actual);
    verify(validator, times(1)).existsBy(expected.getId(), "err.account.find.notFound");
    verify(repository, times(1)).save(expected);
  }

  @Test
  void givenId_whenDelete_thenReturnEntity() {
    TodoListEntity expected = TestDataFactory.getTodoList();

    service.delete(expected.getId());

    verify(validator, times(1)).existsBy(expected.getId(), "err.account.find.notFound");
    verify(repository, times(1)).deleteById(expected.getId());
  }
}
