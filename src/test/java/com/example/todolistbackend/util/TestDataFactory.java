package com.example.todolistbackend.util;

import com.example.todolistbackend.account.entity.AccountEntity;
import com.example.todolistbackend.list.dto.TodoItemEntry;
import com.example.todolistbackend.list.dto.TodoListCreateRequest;
import com.example.todolistbackend.list.dto.TodoListUpdateRequest;
import com.example.todolistbackend.list.entity.TodoItemEntity;
import com.example.todolistbackend.list.entity.TodoListEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TestDataFactory {
  public static AccountEntity getAccount() {
    return new AccountEntity()
        .setId(1L)
        .setName("Petersonas")
        .setCreation(LocalDateTime.of(2022, 12, 1, 0, 0));
  }

  public static TodoListEntity getTodoList() {
    TodoListEntity list = new TodoListEntity();
    return list.setId(1L)
        .setName("Pirkinių sąrašas")
        .setAccount(getAccount().setToDoLists(List.of(list)))
        .setCreated(LocalDateTime.of(2023, 2, 11, 15, 0))
        .setUpdated(LocalDateTime.of(2023, 2, 11, 15, 0))
        .setItems(
            new ArrayList<>(
                List.of(
                    new TodoItemEntity()
                        .setId(1L)
                        .setLabel("Pienas")
                        .setChecked(true)
                        .setToDoList(list),
                    new TodoItemEntity()
                        .setId(2L)
                        .setLabel("Varškė")
                        .setChecked(false)
                        .setToDoList(list))));
  }

  public static TodoListCreateRequest getTodoListCreateRequest() {
    return new TodoListCreateRequest("Naujas", 1L);
  }

  public static TodoListUpdateRequest getTodoListUpdateRequest() {
    return new TodoListUpdateRequest(
        1L, "Naujas", 1L, List.of(new TodoItemEntry(1L, "Jogurtas", false)));
  }
}
