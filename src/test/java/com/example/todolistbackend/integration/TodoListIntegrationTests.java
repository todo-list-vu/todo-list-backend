package com.example.todolistbackend.integration;

import com.example.todolistbackend.list.dto.TodoListCreateRequest;
import com.example.todolistbackend.list.dto.TodoListUpdateRequest;
import com.example.todolistbackend.util.IntegrationTest;
import com.example.todolistbackend.util.TestDataFactory;
import org.junit.jupiter.api.Test;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TodoListIntegrationTests extends IntegrationTest {
  public static final String TODO_LIST_ENDPOINT = "/api/todo-list";

  @Test
  public void findAll() throws Exception {
    performGet(TODO_LIST_ENDPOINT)
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.[0].name").value("Produktų sąrašas"))
        .andExpect(jsonPath("$.[0].created").value("2023-02-11T10:30:00"))
        .andExpect(jsonPath("$.[0].updated").value("2023-02-11T10:32:00"))
        .andExpect(jsonPath("$.[0].items.[0].label").value("Pienas"))
        .andExpect(jsonPath("$.[0].items.[0].checked").value(false))
        .andExpect(jsonPath("$.[0].items.[1].label").value("Agurkai"))
        .andExpect(jsonPath("$.[0].items.[1].checked").value(true))
        .andExpect(jsonPath("$.[0].items.[2].label").value("Kiaušiniai"))
        .andExpect(jsonPath("$.[0].items.[2].checked").value(true))
        .andExpect(jsonPath("$.[1].name").value("Nupirkti"))
        .andExpect(jsonPath("$.[1].created").value("2023-02-11T13:30:00"))
        .andExpect(jsonPath("$.[1].updated").value("2023-02-11T13:30:00"))
        .andExpect(jsonPath("$.[1].items.[0].label").value("Lemputė 40W"))
        .andExpect(jsonPath("$.[1].items.[0].checked").value(false))
        .andExpect(jsonPath("$.[2].name").value("Kažkas"))
        .andExpect(jsonPath("$.[2].created").value("2023-02-15T10:30:00"))
        .andExpect(jsonPath("$.[2].updated").value("2023-02-15T10:30:00"))
        .andExpect(jsonPath("$.[2].items.[0].label").value("Test"))
        .andExpect(jsonPath("$.[2].items.[0].checked").value(true))
        .andExpect(jsonPath("$.[3].name").value("Kietasis"))
        .andExpect(jsonPath("$.[3].created").value("2023-02-15T17:30:00"))
        .andExpect(jsonPath("$.[3].updated").value("2023-02-15T17:30:00"))
        .andExpect(jsonPath("$.[3].items.[0].label").value("Kietas?"))
        .andExpect(jsonPath("$.[3].items.[0].checked").value(false));
  }

  @Test
  public void successful_findById() throws Exception {
    performGet(TODO_LIST_ENDPOINT + "/1")
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("Produktų sąrašas"))
        .andExpect(jsonPath("$.created").value("2023-02-11T10:30:00"))
        .andExpect(jsonPath("$.updated").value("2023-02-11T10:32:00"))
        .andExpect(jsonPath("$.items.[0].label").value("Pienas"))
        .andExpect(jsonPath("$.items.[0].checked").value(false))
        .andExpect(jsonPath("$.items.[1].label").value("Agurkai"))
        .andExpect(jsonPath("$.items.[1].checked").value(true))
        .andExpect(jsonPath("$.items.[2].label").value("Kiaušiniai"))
        .andExpect(jsonPath("$.items.[2].checked").value(true));
  }

  @Test
  public void successful_create() throws Exception {
    TodoListCreateRequest request = TestDataFactory.getTodoListCreateRequest();

    performPost(TODO_LIST_ENDPOINT, request)
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value(request.getName()))
        .andExpect(jsonPath("$.items.length()").value(0));
  }

  @Test
  public void successful_update() throws Exception {
    TodoListUpdateRequest request = TestDataFactory.getTodoListUpdateRequest();

    performPut(TODO_LIST_ENDPOINT + "/" + request.getId(), request)
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(request.getId()))
        .andExpect(jsonPath("$.name").value(request.getName()))
        .andExpect(jsonPath("$.created").value("2023-02-11T10:30:00"))
        .andExpect(jsonPath("$.items.length()").value(1))
        .andExpect(jsonPath("$.items.[0].label").value("Jogurtas"))
        .andExpect(jsonPath("$.items.[0].checked").value(false));
  }

  @Test
  public void successful_delete() throws Exception {
    performDelete(TODO_LIST_ENDPOINT + "/1").andExpect(status().isOk());
    performGet(TODO_LIST_ENDPOINT + "/1").andExpect(status().isNotFound());
  }
}
