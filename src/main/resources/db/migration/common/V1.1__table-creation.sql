CREATE TABLE account
(
    id       SERIAL PRIMARY KEY,
    name     TEXT      NOT NULL,
    creation TIMESTAMP NOT NULL
);

CREATE TABLE todo_list
(
    id         SERIAL PRIMARY KEY,
    name       TEXT      NOT NULL,
    created    TIMESTAMP NOT NULL,
    updated    TIMESTAMP NOT NULL,
    account_id INTEGER   NOT NULL
);

CREATE TABLE todo_item
(
    id           SERIAL PRIMARY KEY,
    todo_list_id INTEGER NOT NULL,
    label        TEXT    NOT NULL,
    checked      BOOLEAN NOT NULL
);

ALTER TABLE todo_list
    ADD FOREIGN KEY (account_id) REFERENCES account (id);

ALTER TABLE todo_item
    ADD FOREIGN KEY (todo_list_id) REFERENCES todo_list (id);