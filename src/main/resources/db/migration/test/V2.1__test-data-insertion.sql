INSERT INTO account (name, creation)
VALUES ('pj purvinis', '2022-12-01T12:00:00'),
       ('Petras', '2022-12-05T12:00:00');

INSERT INTO todo_list (name, created, updated, account_id)
VALUES ('Produktų sąrašas', '2023-02-11T10:30:00', '2023-02-11T10:32:00', 1),
       ('Nupirkti', '2023-02-11T13:30:00', '2023-02-11T13:30:00', 1),
       ('Kažkas', '2023-02-15T10:30:00', '2023-02-15T10:30:00', 2),
       ('Kietasis', '2023-02-15T17:30:00', '2023-02-15T17:30:00', 2);

INSERT INTO todo_item (todo_list_id, label, checked)
VALUES (1, 'Pienas', false),
       (1, 'Agurkai', true),
       (1, 'Kiaušiniai', true),
       (2, 'Lemputė 40W', false),
       (3, 'Test', true),
       (4, 'Kietas?', false);