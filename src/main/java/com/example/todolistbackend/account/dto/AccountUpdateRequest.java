package com.example.todolistbackend.account.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class AccountUpdateRequest {
  @NotNull private Long id;

  @NotNull
  @Size(max = 255)
  private String name;

  @NotNull private LocalDateTime creation;
}
