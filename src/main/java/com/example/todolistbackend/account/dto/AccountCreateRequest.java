package com.example.todolistbackend.account.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class AccountCreateRequest {
  @NotNull
  @Size(max = 255)
  private String name;
}
