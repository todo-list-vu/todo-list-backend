package com.example.todolistbackend.account.dto;

import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class AccountResponse {
  private Long id;
  private String name;
  private LocalDateTime creation;
}
