package com.example.todolistbackend.account.entity;

import com.example.todolistbackend.list.entity.TodoListEntity;
import jakarta.persistence.*;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "account")
public class AccountEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;
  private LocalDateTime creation;

  @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
  @ToString.Exclude
  private List<TodoListEntity> toDoLists = new ArrayList<>();

  public Long getId() {
    return id;
  }

  public AccountEntity setId(Long id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public AccountEntity setName(String name) {
    this.name = name;
    return this;
  }

  public LocalDateTime getCreation() {
    return creation;
  }

  public AccountEntity setCreation(LocalDateTime creation) {
    this.creation = creation;
    return this;
  }

  public List<TodoListEntity> getToDoLists() {
    return toDoLists;
  }

  public AccountEntity setToDoLists(List<TodoListEntity> toDoLists) {
    this.toDoLists = toDoLists;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof AccountEntity entity)) return false;
    return Objects.equals(getName(), entity.getName());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName());
  }

  @Override
  public String toString() {
    return "AccountEntity{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", creation=" + creation +
            ", toDoLists=" + toDoLists +
            '}';
  }
}
