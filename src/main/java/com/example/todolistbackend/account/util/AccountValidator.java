package com.example.todolistbackend.account.util;

import com.example.todolistbackend.account.AccountRepository;
import com.example.todolistbackend.exception.ApiException;
import org.springframework.stereotype.Service;

@Service
public class AccountValidator {
  private final AccountRepository repository;

  public AccountValidator(AccountRepository repository) {
    this.repository = repository;
  }

  public void existsBy(Long id, String message) {
    if (!repository.existsById(id)) {
      throw ApiException.notFound(message).addLabel("id", id);
    }
  }

  public void notExistsBy(String name, String message) {
    if (repository.existsByName(name)) {
      throw ApiException.conflict(message).addLabel("name", name);
    }
  }
}
