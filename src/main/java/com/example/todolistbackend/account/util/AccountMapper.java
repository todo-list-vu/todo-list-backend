package com.example.todolistbackend.account.util;

import com.example.todolistbackend.account.dto.AccountCreateRequest;
import com.example.todolistbackend.account.dto.AccountResponse;
import com.example.todolistbackend.account.dto.AccountUpdateRequest;
import com.example.todolistbackend.account.entity.AccountEntity;

import java.time.LocalDateTime;

public class AccountMapper {
  public static AccountResponse toResponse(AccountEntity entity) {
    return new AccountResponse(entity.getId(), entity.getName(), entity.getCreation());
  }

  public static AccountEntity toEntity(AccountUpdateRequest request) {
    return new AccountEntity()
        .setId(request.getId())
        .setName(request.getName())
        .setCreation(request.getCreation());
  }

  public static AccountEntity toEntity(AccountCreateRequest request) {
    return new AccountEntity().setName(request.getName()).setCreation(LocalDateTime.now());
  }
}
