package com.example.todolistbackend.account;

import com.example.todolistbackend.account.entity.AccountEntity;
import com.example.todolistbackend.account.util.AccountValidator;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public record AccountService(AccountRepository repository, AccountValidator validator) {
    private final static String ERROR_PREFIX = "err.account.";
    public List<AccountEntity> findAll() {
        return repository.findAll();
    }

    public AccountEntity findById(Long id) {
        validator.existsBy(id, ERROR_PREFIX + "find.notFound");
        return repository.findById(id).orElse(null);
    }

    public AccountEntity create(AccountEntity entity) {
        validator.notExistsBy(entity.getName(), ERROR_PREFIX + "create.duplicate");
        return repository.save(entity);
    }

    public AccountEntity update(AccountEntity entity) {
        validator.existsBy(entity.getId(), ERROR_PREFIX + "find.notFound");
        return repository.save(entity);
    }

    public void delete(Long id) {
        validator.existsBy(id, ERROR_PREFIX + "find.notFound");
        repository.deleteById(id);
    }
}
