package com.example.todolistbackend.account;

import com.example.todolistbackend.account.dto.AccountCreateRequest;
import com.example.todolistbackend.account.dto.AccountResponse;
import com.example.todolistbackend.account.dto.AccountUpdateRequest;
import com.example.todolistbackend.account.entity.AccountEntity;
import com.example.todolistbackend.account.util.AccountMapper;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/account")
public class AccountController {
  private final AccountService service;

  public AccountController(AccountService service) {
    this.service = service;
  }

  @Operation(description = "Find all accounts")
  @GetMapping
  public List<AccountResponse> findAll() {
    return service.findAll().stream().map(AccountMapper::toResponse).collect(Collectors.toList());
  }

  @Operation(description = "Find account by id")
  @GetMapping("/{id}")
  public AccountResponse findById(@PathVariable Long id) {
    return AccountMapper.toResponse(service.findById(id));
  }

  @Operation(description = "Create an account")
  @PostMapping
  public AccountResponse create(@RequestBody @Valid AccountCreateRequest request) {
    AccountEntity entity = AccountMapper.toEntity(request);
    return AccountMapper.toResponse(service.create(entity));
  }

  @Operation(description = "Update the account")
  @PutMapping
  public AccountResponse update(@RequestBody @Valid AccountUpdateRequest request) {
    AccountEntity entity = AccountMapper.toEntity(request);
    return AccountMapper.toResponse(service.update(entity));
  }

  @Operation(description = "Delete the account")
  @DeleteMapping("/{id}")
  public void delete(@PathVariable Long id) {
    service.delete(id);
  }
}
