package com.example.todolistbackend.list.dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class TodoListResponse {
  private Long id;
  private String name;
  private LocalDateTime created;
  private LocalDateTime updated;
  private List<TodoItemResponse> items = new ArrayList<>();
}
