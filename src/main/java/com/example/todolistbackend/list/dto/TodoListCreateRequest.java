package com.example.todolistbackend.list.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class TodoListCreateRequest {
  @NotNull
  @Size(max = 255)
  private String name;

  @NotNull private Long accountId;
}
