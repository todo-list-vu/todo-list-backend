package com.example.todolistbackend.list.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class TodoItemEntry {
  private Long id;

  @NotNull
  @Size(max = 500)
  private String label;

  @NotNull private Boolean checked;
}
