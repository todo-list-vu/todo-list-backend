package com.example.todolistbackend.list.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class TodoListUpdateRequest {
  @NotNull private Long id;

  @NotNull
  @Size(max = 255)
  private String name;

  @NotNull private Long accountId;

  @NotNull private List<TodoItemEntry> items = new ArrayList<>();
}
