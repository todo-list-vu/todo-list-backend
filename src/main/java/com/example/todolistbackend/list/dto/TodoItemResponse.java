package com.example.todolistbackend.list.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class TodoItemResponse {
  private Long id;
  private String label;
  private Boolean checked;
}
