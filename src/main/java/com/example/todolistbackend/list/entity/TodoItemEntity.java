package com.example.todolistbackend.list.entity;

import jakarta.persistence.*;

import java.util.Objects;

@Entity(name = "todo_item")
public class TodoItemEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String label;
    private Boolean checked;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "todo_list_id", referencedColumnName = "id")
    private TodoListEntity toDoList;

    public Long getId() {
        return id;
    }

    public TodoItemEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public TodoItemEntity setLabel(String value) {
        this.label = value;
        return this;
    }

    public Boolean getChecked() {
        return checked;
    }

    public TodoItemEntity setChecked(Boolean checked) {
        this.checked = checked;
        return this;
    }

    public TodoListEntity getToDoList() {
        return toDoList;
    }

    public TodoItemEntity setToDoList(TodoListEntity toDoList) {
        this.toDoList = toDoList;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TodoItemEntity toDoItem)) return false;
        return Objects.equals(getLabel(), toDoItem.getLabel()) && Objects.equals(getChecked(), toDoItem.getChecked());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLabel(), getChecked());
    }

    @Override
    public String toString() {
        return "ToDoItem{" +
                "value='" + label + '\'' +
                ", checked=" + checked +
                '}';
    }
}
