package com.example.todolistbackend.list.entity;

import com.example.todolistbackend.account.entity.AccountEntity;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "todo_list")
public class TodoListEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;
  private LocalDateTime created;
  private LocalDateTime updated;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "account_id", referencedColumnName = "id")
  private AccountEntity account;

  @OneToMany(mappedBy = "toDoList", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<TodoItemEntity> items = new ArrayList<>();

  public Long getId() {
    return id;
  }

  public TodoListEntity setId(Long id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public TodoListEntity setName(String name) {
    this.name = name;
    return this;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public TodoListEntity setCreated(LocalDateTime created) {
    this.created = created;
    return this;
  }

  public LocalDateTime getUpdated() {
    return updated;
  }

  public TodoListEntity setUpdated(LocalDateTime updated) {
    this.updated = updated;
    return this;
  }

  public AccountEntity getAccount() {
    return account;
  }

  public TodoListEntity setAccount(AccountEntity account) {
    this.account = account;
    return this;
  }

  public List<TodoItemEntity> getItems() {
    return items;
  }

  public TodoListEntity setItems(List<TodoItemEntity> items) {
    this.items = items;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof TodoListEntity that)) return false;
    return Objects.equals(getName(), that.getName()) && Objects.equals(getCreated(), that.getCreated()) && Objects.equals(getUpdated(), that.getUpdated());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName(), getCreated(), getUpdated());
  }

  @Override
  public String toString() {
    return "ToDoListEntity{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", created=" + created +
            ", updated=" + updated +
            '}';
  }
}
