package com.example.todolistbackend.list;

import com.example.todolistbackend.list.entity.TodoListEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoListRepository extends JpaRepository<TodoListEntity, Long> {
  Boolean existsByName(String name);
}
