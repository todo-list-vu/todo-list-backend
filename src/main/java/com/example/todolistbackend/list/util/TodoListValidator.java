package com.example.todolistbackend.list.util;

import com.example.todolistbackend.exception.ApiException;
import com.example.todolistbackend.list.TodoListRepository;
import org.springframework.stereotype.Service;

@Service
public class TodoListValidator {
  private final TodoListRepository repository;

  public TodoListValidator(TodoListRepository repository) {
    this.repository = repository;
  }

  public void existsBy(Long id, String message) {
    if (!repository.existsById(id)) {
      throw ApiException.notFound(message).addLabel("id", id);
    }
  }

  public void notExistsBy(String name, String message) {
    if (repository.existsByName(name)) {
      throw ApiException.conflict(message).addLabel("name", name);
    }
  }
}
