package com.example.todolistbackend.list.util;

import com.example.todolistbackend.list.dto.TodoItemEntry;
import com.example.todolistbackend.list.dto.TodoItemResponse;
import com.example.todolistbackend.list.entity.TodoItemEntity;
import com.example.todolistbackend.list.entity.TodoListEntity;

import java.util.List;
import java.util.stream.Collectors;

public class TodoItemMapper {
  public static TodoItemResponse toResponse(TodoItemEntity entity) {
    return new TodoItemResponse(entity.getId(), entity.getLabel(), entity.getChecked());
  }

  public static List<TodoItemResponse> toResponses(List<TodoItemEntity> entities) {
    return entities.stream().map(TodoItemMapper::toResponse).collect(Collectors.toList());
  }

  public static TodoItemEntity toEntity(TodoItemEntry request, TodoListEntity todoList) {
    return new TodoItemEntity()
        .setId(request.getId())
        .setLabel(request.getLabel())
        .setChecked(request.getChecked())
        .setToDoList(todoList);
  }

  public static List<TodoItemEntity> toEntities(
      List<TodoItemEntry> entries, TodoListEntity todoList) {
    return entries.stream().map(entry -> toEntity(entry, todoList)).collect(Collectors.toList());
  }
}
