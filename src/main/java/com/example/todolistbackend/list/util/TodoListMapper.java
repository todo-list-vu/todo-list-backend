package com.example.todolistbackend.list.util;

import com.example.todolistbackend.account.entity.AccountEntity;
import com.example.todolistbackend.list.dto.TodoListCreateRequest;
import com.example.todolistbackend.list.dto.TodoListResponse;
import com.example.todolistbackend.list.dto.TodoListUpdateRequest;
import com.example.todolistbackend.list.entity.TodoListEntity;

import java.time.LocalDateTime;

public class TodoListMapper {
  public static TodoListResponse toResponse(TodoListEntity entity) {
    return new TodoListResponse(
        entity.getId(),
        entity.getName(),
        entity.getCreated(),
        entity.getUpdated(),
        TodoItemMapper.toResponses(entity.getItems()));
  }

  public static TodoListEntity toEntity(TodoListUpdateRequest request) {
    TodoListEntity list = new TodoListEntity();
    return list.setId(request.getId())
        .setName(request.getName())
        .setUpdated(LocalDateTime.now())
        .setAccount(new AccountEntity().setId(request.getAccountId()))
        .setItems(TodoItemMapper.toEntities(request.getItems(), list));
  }

  public static TodoListEntity toEntity(TodoListCreateRequest request) {
    return new TodoListEntity()
        .setName(request.getName())
        .setCreated(LocalDateTime.now())
        .setUpdated(LocalDateTime.now())
        .setAccount(new AccountEntity().setId(request.getAccountId()));
  }
}
