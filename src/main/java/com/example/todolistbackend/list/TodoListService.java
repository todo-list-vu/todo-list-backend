package com.example.todolistbackend.list;

import com.example.todolistbackend.list.entity.TodoListEntity;
import com.example.todolistbackend.list.util.TodoListValidator;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoListService {
  private static final String ERROR_PREFIX = "err.account.";
  private final TodoListRepository repository;
  private final TodoListValidator validator;

  public TodoListService(TodoListRepository repository, TodoListValidator validator) {
    this.repository = repository;
    this.validator = validator;
  }

  public List<TodoListEntity> findAll() {
    return repository.findAll();
  }

  public TodoListEntity findById(Long id) {
    validator.existsBy(id, ERROR_PREFIX + "find.notFound");
    return repository.findById(id).orElse(null);
  }

  public TodoListEntity create(TodoListEntity entity) {
    validator.notExistsBy(entity.getName(), ERROR_PREFIX + "create.duplicate");
    return repository.save(entity);
  }

  public TodoListEntity update(TodoListEntity entity) {
    TodoListEntity original = findById(entity.getId());
    original.setUpdated(entity.getUpdated()).setName(entity.getName());
    original.getItems().clear();
    original.getItems().addAll(entity.getItems());
    return repository.save(original);
  }

  public void delete(Long id) {
    validator.existsBy(id, ERROR_PREFIX + "find.notFound");
    repository.deleteById(id);
  }
}
