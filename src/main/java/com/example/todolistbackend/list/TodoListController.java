package com.example.todolistbackend.list;

import com.example.todolistbackend.list.dto.TodoListCreateRequest;
import com.example.todolistbackend.list.dto.TodoListResponse;
import com.example.todolistbackend.list.dto.TodoListUpdateRequest;
import com.example.todolistbackend.list.entity.TodoListEntity;
import com.example.todolistbackend.list.util.TodoListMapper;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/todo-list")
public class TodoListController {
  private final TodoListService service;

  public TodoListController(TodoListService service) {
    this.service = service;
  }

  @Operation(description = "Find all todo lists")
  @GetMapping
  public List<TodoListResponse> findAll() {
    return service.findAll().stream().map(TodoListMapper::toResponse).collect(Collectors.toList());
  }

  @Operation(description = "Find a todo list by id")
  @GetMapping("/{id}")
  public TodoListResponse findById(@PathVariable Long id) {
    return TodoListMapper.toResponse(service.findById(id));
  }

  @Operation(description = "Create a todo list")
  @PostMapping
  public TodoListResponse create(@RequestBody @Valid TodoListCreateRequest request) {
    TodoListEntity entity = TodoListMapper.toEntity(request);
    return TodoListMapper.toResponse(service.create(entity));
  }

  @Operation(description = "Update the todo list")
  @PutMapping("/{id}")
  public TodoListResponse update(
      @RequestBody @Valid TodoListUpdateRequest request, @PathVariable Long id) {
    TodoListEntity entity = TodoListMapper.toEntity(request).setId(id);
    return TodoListMapper.toResponse(service.update(entity));
  }

  @Operation(description = "Delete the todo list")
  @DeleteMapping("/{id}")
  public void delete(@PathVariable Long id) {
    service.delete(id);
  }
}
